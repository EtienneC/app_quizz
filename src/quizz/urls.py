from django.urls import path

from .views import HomePageView, QuizzView, ResultView

urlpatterns = [
    path("result/", ResultView.as_view(), name="result"),
    path("quizz/", QuizzView.as_view(), name="quizz"),
    path("", HomePageView.as_view(), name="home")
]

