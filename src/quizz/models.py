from django.db import models
from django.contrib import admin


class Player(models.Model):
    pseudo = models.CharField(max_length=30)
    score = models.IntegerField(primary_key=False)


class PlayerAdmin(admin.ModelAdmin):
    list_display = ('pseudo', 'score')
    list_filter = ('pseudo',)
    search_fields = ['pseudo']