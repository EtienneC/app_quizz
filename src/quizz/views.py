from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import TemplateView
from .models import Player
import json
import html
import random
import requests


class HomePageView(TemplateView):
    template_name = "home.html"


class QuizzView(TemplateView):
    template_name = "quizz.html"

    def get(self, request):
        res = self.getRequest()
        results = {"questions": res, "json": json.dumps(res)}
        return render(request, self.template_name, results)

    def getRequest(self):
        url = "https://opentdb.com/api.php?amount=10"
        response = requests.get(url)
        data = response.json()
        for q in data["results"]:
            q["answers"] = q["incorrect_answers"]
            q["answers"].append(q["correct_answer"])
            random.shuffle(q["answers"])
        return html.unescape(data["results"])


class ResultView(TemplateView):
    template_name = "result.html"
    http_method_names = ['get','post']

    # Handle POST GTTP requests
    def post(self, request):
        #for key in request.POST:
            #print(f"Clef : {key} et la valeur : {request.POST[key]}")
        score = 0
        reponses = {}
        answer = request.POST["questions"]
        answer = json.loads(answer)
        for key in request.POST:
            for q in answer:
                if html.unescape(q["question"]) == key:
                    if q["correct_answer"] == request.POST[key]:
                        score += 1
        reponses["score"] = score
        reponses["user_name"] = request.POST["user_name"]
        player = Player()
        # faire un constructeur
        player.score = score
        player.pseudo = request.POST["user_name"]
        player.save()

        reponses["best_players"] = Player.objects.order_by("-score")[:3].values()

        return render(request, self.template_name, reponses)
    
  